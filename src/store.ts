import { configureStore } from '@reduxjs/toolkit';
import AccountReducer from './StateManagement/AccountReducer';

export type RootState = ReturnType<typeof store.getState>;

//Создаю настройку для редакса, иницилизирую редюсеры
const store = configureStore({
    reducer: {
        account: AccountReducer,
    },

});
export default store;