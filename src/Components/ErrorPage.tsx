import React from 'react';
import { Stack, Typography } from '@mui/material';

const ErrorPage = ({ errorMessage } : { errorMessage: string }) => {
    return (
        <Stack spacing={4} direction='row' style={{ display: "flex",  alignItems: "center",  justifyContent: "center"    }}>
            <Typography variant='h1'>{errorMessage}</Typography>
        </Stack>
    )
}

export default ErrorPage;