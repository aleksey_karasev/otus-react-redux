
import { Route, BrowserRouter, Routes} from "react-router-dom";

import Login from "./Login";
import Register from "./Register";
import { useState } from 'react';
import { useSelector  } from "react-redux";

import Account from "./Account";
import { RootState } from '../store';

import { CustomButton } from "./Mui/CustomButton";
import { ButtonGroup } from '@mui/material';
import { Stack, Typography } from '@mui/material';


export default function HomePage() {
    const [isLog, setIsLog] = useState(false);

    const accountData = useSelector((state: RootState) => state.account); // Получаем состояние из Redux

    const Logout = (log: boolean) => {
       setIsLog(log);
       console.log(log);
    };

    return (
        
        <BrowserRouter>
            <div>
            <nav>
                {isLog ? (
                    <>
                        <br/>
                        <ButtonGroup style={{ display: "flex",  alignItems: "right",  justifyContent: "right"    }}>
                            <CustomButton text="Ваш профиль" linkTo="/account"></CustomButton>
                            <CustomButton onClick={() => Logout(false)} text="Выйти" linkTo="/HomePage"></CustomButton>
                        </ButtonGroup>

                        <Stack spacing={4} direction='row' style={{ display: "flex",  alignItems: "left",  justifyContent: "left"    }}>
                            <Typography variant='h3'>Добро пожаловать</Typography>
                        </Stack>
                    </>
                ) : (
                    <>
                        <br/>
                        <ButtonGroup style={{ display: "flex",  alignItems: "center",  justifyContent: "center"    }}>
                            <CustomButton text="Войти" linkTo="/login"></CustomButton>
                            <CustomButton text="Зарегестрироваться" linkTo="/register"></CustomButton>
                        </ButtonGroup>

                        <Stack spacing={4} direction='row' style={{ display: "flex",  alignItems: "left",  justifyContent: "left"    }}>
                            <Typography variant='h3'>Портал Redux</Typography>
                        </Stack>
                    </>
                )}
            </nav>
            
            <br />
            <Routes>
                {!isLog && <Route path='login' element={<Login onLogin={Logout} />} />}
                <Route path='register' element={<Register />} /> 
                <Route path='account' element={<Account />} /> 
            </Routes>

            <br />
            <Stack spacing={4} direction='row' style={{ color: "chocolate", display: "flex",  alignItems: "center",  justifyContent: "center"    }}>
                <Typography variant='h3'>В вашей стране "{accountData.account.country}" портал недоступен</Typography>
            </Stack>
            </div>

        </BrowserRouter>
    )
}