import { useDispatch, useSelector  } from "react-redux";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AccountModel } from '../Models/AccountModel';
import  { Actions }  from '../StateManagement/AccountReducer';
import { RootState } from '../store';
import { useState } from 'react';
import { Stack, Typography } from '@mui/material';

import { CustomField } from "./Mui/CustomField";
import { CustomButton } from "./Mui/CustomButton";




const Account = () => {

    const accountData = useSelector((state: RootState) => state.account); // Получаем состояние из Redux
    const dispatch = useDispatch(); //Для использования редюсеров

    const [country, setCountry] = useState('N/A');
    const [age, setAge] = useState('N/A');


    return <div>

        <Stack spacing={4} direction='row' style={{ display: "flex",  alignItems: "center",  justifyContent: "center"    }}>
            <Typography variant='h3'>Страна: {accountData.account.country}</Typography>
        </Stack>
        <br />
        <Stack spacing={4} direction='row' style={{ display: "flex",  alignItems: "center",  justifyContent: "center"    }}>
            <Typography variant='h3'>Возраст: {accountData.account.age}</Typography>
        </Stack>
        <br />

        <CustomField text="Age" onChange={(e) => setAge(e.target.value) }/>
        <br />
        <CustomButton text="Изменить возраст" linkTo="./" onClick={() => dispatch(Actions.setAccountAge(age))}></CustomButton>

        <br />
        <CustomField text="Country" onChange={(e) => setCountry(e.target.value) }/>
        <br />
        <CustomButton text="Изменить страну" linkTo="./" onClick={() => dispatch(Actions.setAccountUpdateCountry(country))}></CustomButton>

    </div>
}

export default Account;