import React, { useState } from 'react';
import ErrorPage from './ErrorPage';
import { CustomField } from "./Mui/CustomField";
import { CustomButton } from "./Mui/CustomButton";

const Register = () => {
    const [error, setError] = useState(false);

    const handleRegister = () => {
        setError(true);
    }

    return <div>

        {error ? (
            <>
            <ErrorPage errorMessage='404'/>
            </>
        ) : (
            <>
            <CustomField text="Имя" />
            <br />
            <CustomField text="Фамилия" />
            <br />
            <CustomField text="Login" />
            <br />
            <CustomField text="Password" />
            <br />
            <CustomButton text="Зарегестрироваться" linkTo="./" onClick={handleRegister}></CustomButton>
            </>
        )};
    </div>
}

export default Register;