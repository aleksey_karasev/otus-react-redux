import React from 'react';
import { Stack, Button, ButtonGroup, TextField } from '@mui/material';
import SendIcon from '@mui/icons-material/Send';
import { Link } from 'react-router-dom';

interface CustomFieldProps {
    onChange?: (e: any) => void;
    text: string;
    error?: boolean;
}

const CustomField: React.FC<CustomFieldProps> = ({ text, onChange, error = false }) => {
    return (
        <Stack spacing={4} direction='row' style={{ display: "flex",  alignItems: "center",  justifyContent: "center"    }}>
            <TextField label={text} error={error} required onChange={onChange}/>
        </Stack>
    );
}


export { CustomField };
