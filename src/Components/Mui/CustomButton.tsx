import React from 'react';
import { Stack, Button, ButtonGroup } from '@mui/material';
import SendIcon from '@mui/icons-material/Send';
import { Link } from 'react-router-dom';

interface CustomButtonProps {
    onClick?: () => void; 
    text: string;
    linkTo:string;
}

const CustomButton: React.FC<CustomButtonProps> = ({ onClick, text, linkTo }) => {
    return (
        <Stack spacing={2} direction='row' style={{ display: "flex",  alignItems: "center",  justifyContent: "center"    }}>
            <Button variant='contained' onClick={onClick}>
                <Link to={linkTo} style={{ textDecoration: 'none', color: 'inherit' }}>
                    {text}
                </Link>
            </Button>
        </Stack>
    );
}


export { CustomButton };
