import React, { useState } from 'react';
import { CustomField } from "./Mui/CustomField";
import { CustomButton } from "./Mui/CustomButton";

const Login = ({ onLogin }: { onLogin: (isLoggedIn: boolean) => void }) => {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [errorLogin, setErrorLogin] = useState(false);
    const [errorPass, setErrorPass] = useState(false);

    const validation = () => {
        if(login.trim().length === 0) {setErrorLogin(true);}
        else {setErrorLogin(false);}

        if(password.trim().length === 0) {setErrorPass(true);}
        else {setErrorPass(false);}
    };
    
    const handleLogin = () => {

        validation();

        if(login === 'admin' && password === 'admin') {
            onLogin(true);
            console.log('Успешный вход');
        }
        else {
            console.log('Вход невыполнен');
        }

    }

    return <div>

        <CustomField error={errorLogin} text="Login" onChange={(e) => setLogin(e.target.value)}/>
        <br />
        <CustomField error={errorPass} text="Password" onChange={(e) => setPassword(e.target.value)}/>
        <br />
        <CustomButton text="Вход" linkTo="./" onClick={handleLogin}></CustomButton>
    </div>
}

export default Login;