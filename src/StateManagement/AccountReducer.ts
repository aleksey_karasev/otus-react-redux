import { AccountModel } from "../Models/AccountModel";

export const Actions = Object.freeze({

    setAccountAge: (age: string) => ({ type: '[ACCOUNT_STATE] UPDATE_AGE', data: age }),

    ageMinus: (account: AccountModel) => ({ type: '[ACCOUNT_STATE] AGE_MINUS', data: account }),

    agePlus: (account: AccountModel) => ({ type: '[ACCOUNT_STATE] AGE_PLUS', data: account }),

    setAccountUpdateCountry: (country: string) => ({ type: '[ACCOUNT_STATE] UPDATE_COUNTRY', data: country }),
});


interface State {
    account: AccountModel;
};

const initialState: State = {
    account: { age: 0, country: 'N/A' }
};

const accountReducer = (state = initialState, action: any) => {

    switch (action.type) {

        case '[ACCOUNT_STATE] UPDATE_AGE':
            return { ...state, account: { ...state.account, age: action.data } };

        case'[ACCOUNT_STATE] AGE_MINUS':
            return { ...state, account: { ...state.account, age: state.account.age - 1 } };

        case '[ACCOUNT_STATE] AGE_PLUS':
            return { ...state, account: { ...state.account, age: state.account.age + 1 } };

        case '[ACCOUNT_STATE] UPDATE_COUNTRY':
            return { ...state, account: { ...state.account, country: action.data }};

        default:
            return state;
    }
};

export default accountReducer;